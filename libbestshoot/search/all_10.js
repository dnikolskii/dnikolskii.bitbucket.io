var searchData=
[
  ['test_5fdataset',['test_dataset',['../namespacemake__dataset__rgb.html#a7f7098b7604ffba46bdd80a0fabf6f34',1,'make_dataset_rgb']]],
  ['test_5fratio',['test_ratio',['../namespacemake__dataset__rgb.html#a062d853959d0b2f6c459cf717b0123c6',1,'make_dataset_rgb']]],
  ['testone',['testone',['../namespacetestone.html',1,'']]],
  ['testone_2epy',['testone.py',['../testone_8py.html',1,'']]],
  ['toomanyfaces',['tooManyFaces',['../namespaceswip.html#ae58e250bbb1871b10e1531b8f7ac4338ab605bb956c847033f4b95674a1998a72',1,'swip']]],
  ['toomanymouths',['tooManyMouths',['../namespaceswip.html#ae58e250bbb1871b10e1531b8f7ac4338a27ea6946868782320b8364c5b99391f3',1,'swip']]],
  ['train_5fdataset',['train_dataset',['../namespacemake__dataset__rgb.html#a91390a13c3823e883881b8b682c8c8b1',1,'make_dataset_rgb']]],
  ['train_5fratio',['train_ratio',['../namespacemake__dataset__rgb.html#a3ad16a1f4812c1fe3b27be652c293de0',1,'make_dataset_rgb']]],
  ['true',['True',['../namespacecutting-3dmask.html#aef65a623bfaee752f159698f4ef33e26',1,'cutting-3dmask.True()'],['../namespacecutting-print.html#a370c89d6c41f26ae4cf162902e3845f5',1,'cutting-print.True()'],['../namespacecutting-real.html#a7979459e4bd3e63188c38bc65430aab6',1,'cutting-real.True()'],['../namespacelinux-rgb.html#a3600890f877bdf3d16e2bff39a143264',1,'linux-rgb.True()']]],
  ['tune',['tune',['../namespacetune.html',1,'']]],
  ['tune_2epy',['tune.py',['../tune_8py.html',1,'']]]
];
