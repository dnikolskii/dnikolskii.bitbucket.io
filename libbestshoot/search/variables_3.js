var searchData=
[
  ['data_5fdirs',['data_dirs',['../namespacecutting-3dmask.html#a3b6e675f7d409fb6aa1c77faea7fbac1',1,'cutting-3dmask.data_dirs()'],['../namespacecutting-print.html#a24a046b97d6e09e9e47869585f467387',1,'cutting-print.data_dirs()'],['../namespacecutting-real.html#aac49f2883b11c05376a954ba9dd753c5',1,'cutting-real.data_dirs()'],['../namespacelinux-rgb.html#ad1979584b76f7623508ff8a44132edec',1,'linux-rgb.data_dirs()'],['../namespacetune.html#ab21f1747469cf2ecddbc1f0484b4af7a',1,'tune.data_dirs()']]],
  ['df',['df',['../namespacecutting-3dmask.html#a7b2eab75e26d99dd4625d686b6e9968b',1,'cutting-3dmask.df()'],['../namespacecutting-print.html#a9eadad4d91bbd284cfa2a0fabbfe08d9',1,'cutting-print.df()'],['../namespacecutting-real.html#a06df23b1e310e1a8fb415f776298027d',1,'cutting-real.df()'],['../namespacetune.html#add65b7e08f7127a242c30105d049d7a4',1,'tune.df()']]],
  ['dst',['DST',['../namespacemake__dataset__rgb.html#add07f6203cf14a427062ac6f2c8457ba',1,'make_dataset_rgb']]],
  ['dst_5fdir',['dst_dir',['../namespacemake__dataset__rgb.html#a3ac2ada4e5c2fa7da3c1d3ac5e3a6ffa',1,'make_dataset_rgb']]]
];
