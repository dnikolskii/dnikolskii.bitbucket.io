var searchData=
[
  ['parents',['parents',['../namespacecutting-3dmask.html#af7dfe1b1464e0d6c256a5c2927016f39',1,'cutting-3dmask.parents()'],['../namespacecutting-print.html#a86e606cf5fcdb3292625a0bebe1ce900',1,'cutting-print.parents()'],['../namespacecutting-real.html#a400e1bb908e35846d89e18987abb2282',1,'cutting-real.parents()'],['../namespacelinux-rgb.html#af84db611d3472e4a6dbde6eba94e2f93',1,'linux-rgb.parents()']]],
  ['pd',['pd',['../namespacecutting-3dmask.html#a52a3b71d183778c0a04895396c76d0fd',1,'cutting-3dmask.pd()'],['../namespacecutting-print.html#a2e13b1747ece3900fe60cfb723abec33',1,'cutting-print.pd()'],['../namespacecutting-real.html#a940d1c95b6b0ebd634643998f0076819',1,'cutting-real.pd()'],['../namespacelinux-rgb.html#ab572cc0272bdf471fd78e6cd348a4d49',1,'linux-rgb.pd()']]],
  ['printsetting',['printSetting',['../classswip_1_1BestShoot.html#a70b3fe2e67ef49556ad3f52ff5c4b2ad',1,'swip::BestShoot::printSetting()'],['../cbestshoot_8h.html#a1a3c8971add6c95915c3904d1370918e',1,'printSetting():&#160;cbestshoot.h']]],
  ['pstr',['pstr',['../namespacetune.html#ac6dfebee75e51d18b0bf0e1120caa0dc',1,'tune']]],
  ['ptr',['ptr',['../namespacecutting-3dmask.html#a6af7d8c20cb326ffafd11ce2039072fd',1,'cutting-3dmask.ptr()'],['../namespacecutting-print.html#ae83f12e43c82a25b793e7e6a896e8686',1,'cutting-print.ptr()'],['../namespacecutting-real.html#a8bdefeb831942fc7bd20b79cfb182be0',1,'cutting-real.ptr()'],['../namespacetestone.html#af026931c62d700b64415b9b2d0195619',1,'testone.ptr()'],['../namespacetune.html#a03c43191d5d377b4f4f288409d50266e',1,'tune.ptr()']]]
];
