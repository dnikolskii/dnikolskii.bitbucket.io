var searchData=
[
  ['s',['s',['../namespacecutting-3dmask.html#a0976b8b1c912baa83d4edde828218a07',1,'cutting-3dmask.s()'],['../namespacecutting-print.html#a9889c5a2ae46018960da9ab9d80d24c4',1,'cutting-print.s()'],['../namespacecutting-real.html#a4ca8e7ad6459acd5987d212502e7b7fb',1,'cutting-real.s()'],['../namespacetune.html#ad37b0dda75a8d6236d7079ecc81134f1',1,'tune.s()']]],
  ['short_5fname',['short_name',['../namespacecutting-3dmask.html#aad253fc8fcbbf5da9b7e83c75c2f9e04',1,'cutting-3dmask.short_name()'],['../namespacecutting-print.html#ad97270c854e5f08fc2fa8a6e92c744fc',1,'cutting-print.short_name()'],['../namespacecutting-real.html#a9394c39a5b505fb4daaabbd4b7a41dc6',1,'cutting-real.short_name()'],['../namespacelinux-rgb.html#a27576aae1709f2b4cb9f502f44b0c9f0',1,'linux-rgb.short_name()'],['../namespacetune.html#aa3f6451589946156efca03be5d307384',1,'tune.short_name()']]],
  ['src',['SRC',['../namespacemake__dataset__rgb.html#ac8cd529e6e981e78a0bcc75f427ccbe9',1,'make_dataset_rgb']]],
  ['src_5fdir',['src_dir',['../namespacemake__dataset__rgb.html#a956da16642a18597d428ec0d8910ac6a',1,'make_dataset_rgb']]],
  ['src_5fshort',['src_short',['../namespacemake__dataset__rgb.html#a92eb34c55852469cdd74e4c16b4407d7',1,'make_dataset_rgb']]],
  ['subimage',['subImage',['../namespacecutting-3dmask.html#a046e31c53877d3ed69a471d1205941b7',1,'cutting-3dmask.subImage()'],['../namespacecutting-real.html#a7e8a382cb5a5e1ef030e91afc411c5b2',1,'cutting-real.subImage()'],['../namespacelinux-rgb.html#a62d306f2724a23dfc15fe291c953a145',1,'linux-rgb.subImage()']]]
];
